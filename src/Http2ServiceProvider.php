<?php
declare(strict_types=1);

namespace Shizzen\Http2;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Config\Repository as Config;

class Http2ServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/http2.php', 'http2'
        );

        $this->app->singleton(Pusher::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(Router $router, Config $config)
    {
        $router->middlewareGroup(
            'http2', $config->get('http2.middleware')
        );

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/http2.php' => config_path('http2.php'),
            ], 'http2-config');
        }
    }
}
