<?php
declare(strict_types=1);

namespace Shizzen\Http2;

use Symfony\Component\WebLink\Link;
use Symfony\Component\WebLink\GenericLinkProvider;

class Pusher
{
    public function __construct(
        protected GenericLinkProvider $linksProvider
    ) {}

    public function getLinksProvider(): GenericLinkProvider
    {
        return $this->linksProvider;
    }

    /**
     * Adds a "Link" HTTP header.
     *
     * @param string $rel        The relation type (e.g. "preload", "prefetch", "prerender" or "dns-prefetch")
     * @param array  $attributes The attributes of this link (e.g. "['as' => true]", "['pr' => 0.5]")
     */
    public function link(string $uri, string $rel, array $attributes = []): static
    {
        $link = new Link($rel, $uri);
        foreach ($attributes as $key => $value) {
            $link = $link->withAttribute($key, $value);
        }

        $this->linksProvider = $this->linksProvider->withLink($link);

        return $this;
    }

    /**
     * Preloads a resource.
     *
     * @param array $attributes The attributes of this link (e.g. "['as' => true]", "['crossorigin' => 'use-credentials']")
     */
    public function preload(string $uri, array $attributes = []): static
    {
        return $this->link($uri, 'preload', $attributes);
    }

    /**
     * Resolves a resource origin as early as possible.
     *
     * @param array $attributes The attributes of this link (e.g. "['as' => true]", "['pr' => 0.5]")
     */
    public function dnsPrefetch(string $uri, array $attributes = []): static
    {
        return $this->link($uri, 'dns-prefetch', $attributes);
    }

    /**
     * Initiates a early connection to a resource (DNS resolution, TCP handshake, TLS negotiation).
     *
     * @param array $attributes The attributes of this link (e.g. "['as' => true]", "['pr' => 0.5]")
     *
     */
    public function preconnect(string $uri, array $attributes = []): static
    {
        return $this->link($uri, 'preconnect', $attributes);
    }

    /**
     * Indicates to the client that it should prefetch this resource.
     *
     * @param array $attributes The attributes of this link (e.g. "['as' => true]", "['pr' => 0.5]")
     */
    public function prefetch(string $uri, array $attributes = []): static
    {
        return $this->link($uri, 'prefetch', $attributes);
    }

    /**
     * Indicates to the client that it should prerender this resource .
     *
     * @param array $attributes The attributes of this link (e.g. "['as' => true]", "['pr' => 0.5]")
     */
    public function prerender(string $uri, array $attributes = []): static
    {
        return $this->link($uri, 'prerender', $attributes);
    }
}
