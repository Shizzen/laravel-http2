<?php
declare(strict_types=1);

namespace Shizzen\Http2\Middleware;

use Closure;
use Shizzen\Http2\Pusher;
use Symfony\Component\WebLink\HttpHeaderSerializer;

class SetLinks
{
    /**
     * Create a new middleware instance.
     * 
     * @param Pusher $pusher The pusher instance
     */
    public function __construct(
        protected readonly Pusher $pusher
    ) {}

    /**
     * Handle an incoming request.
     */
    public function handle($request, Closure $next): mixed
    {
        $response = $next($request);

        $links = $this->pusher->getLinksProvider()->getLinks();

        $serializer = new HttpHeaderSerializer();

        $response->headers->set('Link', $serializer->serialize($links), false);

        return $response;
    }
}
